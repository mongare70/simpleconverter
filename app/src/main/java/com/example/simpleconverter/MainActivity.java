package com.example.simpleconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //The view objects
    EditText editTextInches, editTextMillimeters;
    Button buttonExit, buttonConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextInches = (EditText) findViewById(R.id.editTextInches);
        editTextMillimeters = (EditText) findViewById(R.id.editTextMillimeters);
        buttonConvert = (Button) findViewById(R.id.buttonConvert);
        buttonExit = (Button) findViewById(R.id.buttonExit);

        buttonConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strMillimeters = editTextMillimeters.getText().toString();
                String strInches = editTextInches.getText().toString();

                if (!strMillimeters.equals("") && strInches.equals("")) {
                    double millimeters = Double.valueOf(editTextMillimeters.getText().toString());
                    double inches = millimeters / 25.4;
                    editTextInches.setText(String.valueOf(inches));

                } else if (!strInches.equals("") && strMillimeters.equals("")) {
                    double inches = Double.valueOf(editTextInches.getText().toString());
                    double millimeters = inches * 25.4;
                    editTextMillimeters.setText(String.valueOf(millimeters));
                } else if (!strInches.equals("") && !strMillimeters.equals("")){
                    Toast.makeText(getApplicationContext(), "Please clear the text field first!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }
}
